<?php include("header.php"); ?>
<body>
<div class="col-md-4"></div>
<div class="container col-md-3">
      <form class="form-signin" action="" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
        </div>
        <button class="btn btn-md btn-primary btn-block" type="submit">Sign in</button>
        <p>Forgot your password? Click <a href="#">here</a></p>
      </form>
    </div> <!-- /container -->
<div class="col-md-5"></div>
