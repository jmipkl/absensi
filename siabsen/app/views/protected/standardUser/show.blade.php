@extends('master')

@section('title', 'View Profile')

@section('content')
@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif
	<div class="col-md-3">
		<div class="panel panel-info">
  		<div class="panel-heading">{{ $user->first_name }}'s Profile</div>
  			<div class="panel-body">
  				<div class="col-md-offset-1 col-md-10">
   				 <img src="doto.jpg" class="img-thumbnail">
  				</div>
  				<div class="col-md-12">
  				<ul class="list-group">
  					<li class="list-group-item" data-tooltip="tooltip" title="Email">{{ $user->email }}</li>
  					<li class="list-group-item" data-tooltip="tooltip" title="Firstname">{{ $user->first_name }}</li>
  					<li class="list-group-item" data-tooltip="tooltip" title="Lastname">{{ $user->last_name }}</li>
  				</ul>
  				<p >Navigation</p>
  				<ul class="nav nav-pills nav-stacked">
  					<li role="presentation" class="active" data-toggle="modal" data-target="#edit"><a href="#">Edit Profile</a></li>
				</ul>
  				</div>
  			</div>
		</div>
	</div>
<div id="edit" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<h2 class="panel-title text-center"><strong>Edit Data User</strong></h2>
    	</div>
    	<div class="modal-body">
       	{{ Form::model($user, ['method' => 'PATCH', 'route' => ['profiles.update', $user->id]]) }}

		<!-- email Field -->
		<div class="form-group">
			{{ Form::label('email', 'Email:') }}
			{{ Form::email('email', null, ['class' => 'form-control']) }}
			{{ errors_for('email', $errors) }}
		</div>


		<!-- first_name Field -->
		<div class="form-group">
			{{ Form::label('first_name', 'First Name:') }}
			{{ Form::text('first_name', null, ['class' => 'form-control']) }}
			{{ errors_for('first_name', $errors) }}
		</div>

		<!-- last_name Field -->
		<div class="form-group">
			{{ Form::label('last_name', 'Last Name:') }}
			{{ Form::text('last_name', null, ['class' => 'form-control']) }}
			{{ errors_for('last_name', $errors) }}

		</div>

		<!-- Password field -->
		<div class="form-group">
			{{ Form::label('password', 'Password:') }}
			{{ Form::password('password', ['class' => 'form-control']) }}
			<p class="help-block">Leave password blank to NOT edit the password.</p>
			{{ errors_for('password', $errors) }}
		</div>

		<!-- Password Confirmation field -->
		<div class="form-group">
			{{ Form::label('password_confirmation', 'Repeat Password:') }}
			{{ Form::password('password_confirmation', ['class' => 'form-control'] )}}
		</div>


		<!-- Update Profile Field -->
		<div class="form-group">
			{{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
		</div>
	{{ Form::close() }}
    	</div>
    </div>
  </div>
</div>
	@if(Sentry::check())

	@endif

@stop