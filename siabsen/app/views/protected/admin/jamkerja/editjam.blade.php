@extends('protected.admin.master')

@section('title', 'Edit Data Jam Kerja')

@section('content')
	
	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Ubah Data Jam Kerja</small>
        </h1>
        
 
        {{ Form::model($jambyid, array('route' => array('admin.jamkerja.update', $jambyid->idjam),'method' => 'PUT')) }}
             
         <div class="form-group">
                {{ Form::label('nama_jamkerja', 'Nama Jam Kerja') }}
                {{ Form::text('nama_jamkerja', null, array('class' => 'form-control','placeholder'=>'masukkan nama jam kerja')) }}
                {{ '<div>'.$errors->first('nama_jamkerja').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('jam_masuk', 'Jam Masuk') }} <br />
                {{ Form::input('time', 'jam_masuk') }}
                {{ '<div>'.$errors->first('jam_masuk').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('jam_pulang', 'Jam Pulang') }} <br />
                {{ Form::input('time', 'jam_pulang') }}
                {{ '<div>'.$errors->first('jam_pulang').'</div>' }}
            </div>
            
             <div class="form-group">
                {{ Form::label('start_isht', 'Mulai Istirahat') }} <br />
                {{ Form::input('time', 'start_isht') }}
                {{ '<div>'.$errors->first('start_isht').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('end_isht', 'Selesai Istirahat') }} <br />
                {{ Form::input('time', 'end_isht') }}
                {{ '<div>'.$errors->first('end_isht').'</div>' }}
            </div>
             
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
 
    </div>
</div>

@stop