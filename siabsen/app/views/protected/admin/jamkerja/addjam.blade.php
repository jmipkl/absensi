@extends('protected.admin.master')

@section('title', 'Jam Kerja')

@section('content')

@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Jam Kerja Pegawai</small>
        </h1>
        @if (Session::has('message'))
            {{ Session::get('message') }}
        @endif
        <p class="col-lg-2"><button data-target="#newjam" data-toggle="modal" data-tooltip="tooltip" title="Tambah Jam" class="btn btn-primary btn-block"><i class="fa fa-clock-o fa-lg"></i></button></p>
        @include('includes.jam.create')
        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                   
                                        <th class="text-center" rowspan="2">Nama Jam Kerja</th>
                                        <th class="text-center" rowspan="2">Jam Masuk</th>
                                        <th class="text-center" rowspan="2">Jam Pulang</th>
                                        <th class="text-center" colspan="2">Istirahat</th>                                        
										<th class="text-center" rowspan="2" width="146">Aksi</th>
                                        
                                    </tr>
                                    <tr  align="center">
                                    <td>Mulai</td>
                                    <td>Selesai</td>
                                    </tr>
                                    
                                    
                                </thead>
                                <tbody>
                                @foreach($jamkerja as $value)
                                    <tr>
                                        
                                        <td>{{{ $value->nama_jamkerja }}}</td>
                                        <td>{{{ $value->jam_masuk }}}</td>
									    <td>{{{ $value->jam_pulang }}}</td>
                                        <td>{{{ $value->start_isht }}}</td>
                                        <td>{{{ $value->end_isht }}}</td>
                                       
										
                                        <td class="text-center">
                                            <div class="btn-group">
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$value->idjam}}" data-tooltip="tooltip" title="edit"><i class="fa fa-pencil-square-o"></i></button>
                                            @include('includes.jam.edit')
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$value->idjam}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                            @include('includes.jam.delete')
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach   
                                </tbody>
                            </table>
            </div>
            {{ $jamkerja->links(); }}
 
  </div>
</div>

@stop