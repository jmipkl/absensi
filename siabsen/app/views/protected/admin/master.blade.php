<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>@yield('title') - Admin - SI Absensi</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Stylesheets -->
	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::style('assets/font-awesome-4.3.0/css/font-awesome.min.css') }}
	{{ HTML::style('assets/css/sweet-alert.css') }}

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--Js config-->
	{{ HTML::script('assets/js/jquery-1.11.0.js') }}
	{{ HTML::script('assets/js/bootstrap.min.js') }}
	{{ HTML::script('assets/js/sweet-alert.js') }}
	<script type="text/javascript">
    $(function () {$('[data-tooltip="tooltip"]').tooltip()});
    $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()});
  	</script>
</head>
<body>

	<header>

		<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/">Signed as Admin</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="{{ set_active_admin('admin') }}"><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
		      </ul>

		      <ul class="nav navbar-nav navbar-right">

		        <li><a href="/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>Logout</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
	@include('includes.sidebar')

	<div class="container col-md-offset-2 col-md-10">
		@yield('content')
	</div>
</body>
</html>