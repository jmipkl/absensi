@extends('protected.admin.master')

@section('title', 'Edit Mesin')

@section('content')
	
	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	<div class="row">
    <div class="col-md-6">
        <h1 class="page-header">
            Halaman
            <small>Ubah Data Mesin</small>
        </h1>
        
 
        {{ Form::model($mesinbyid, array('route' => array('admin.mesin.update', $mesinbyid->id),'method' => 'PUT')) }}
             
            <div class="form-group">
                {{ Form::label('nama_mesin', 'Nama Mesin') }}
                {{ Form::text('nama_mesin', null, array('class' => 'form-control','placeholder'=>'masukkan nama mesin')) }}
                {{ '<div>'.$errors->first('nama_mesin').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('IP', 'Alamat IP') }}
                {{ Form::text('IP', null, array('class' => 'form-control','placeholder'=>'masukkan alamat IP')) }}
                {{ '<div>'.$errors->first('IP').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('port', 'Port') }}
                {{ Form::text('port', null, array('class' => 'form-control','placeholder'=>'masukkan port')) }}
                {{ '<div>'.$errors->first('port').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('merk', 'Jenis/Merk Mesin') }}
                {{ Form::text('merk', null, array('class' => 'form-control','placeholder'=>'masukkan jenis/merk mesin')) }}
                {{ '<div>'.$errors->first('merk').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('status', 'Status') }}<br />
				{{ Form::radio('status', true) }} AKTIF <br />
				{{ Form::radio('status', false) }} TIDAK AKTIF
                {{ '<div>'.$errors->first('status').'</div>' }}
            </div>
             
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
 
    </div>
</div>

@stop