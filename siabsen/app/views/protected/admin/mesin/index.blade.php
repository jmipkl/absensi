@extends('protected.admin.master')

@section('title', 'Machine Management')

@section('content')

@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Manajemen Mesin Absensi</small>
        </h1>
        @if (Session::has('message'))
            {{ Session::get('message') }}
        @endif
        <p class="col-md-2"><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#newmesin" role="button" data-tooltip="tooltip" title="Tambah Mesin"><i class="fa fa-cog fa-lg"></i> <i class="fa fa-plus"></i></button></p>
        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>Nama Mesin</th>
                                        <th>Alamat IP</th>
                                        <th>Port</th>
                                        <th>Jenis/Merk Mesin</th>
                                        <th>Status</th>
                                        <th width="146">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($mesin as $value)
                                    <tr>
                                        
                                        <td>{{{ $value->nama_mesin }}}</td>
                                        <td>{{{ $value->IP }}}</td>
									    <td>{{{ $value->port }}}</td>
                                        <td>{{{ $value->merk }}}</td>
										<td>
                                            @if($value->status==1)
                                            <span class="label label-success" data-tooltip="tooltip" data-placement="top" title="ON">{{ 'Active' }}</span>
                                            @else
                                            <span class="label label-danger" data-tooltip="tooltip" data-placement="top" title="OFF">{{ 'Not Active' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$value->id}}" data-tooltip="tooltip" title="edit"><i class="fa fa-pencil-square-o"></i></button>
                                            @include('includes.mesin.edit')
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$value->id}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                            @include('includes.mesin.delete')
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach   
                                </tbody>

                            </table>
            </div>
            {{$mesin->links()}}
            @include('includes.mesin.create')
 
    </div>
</div>

@stop