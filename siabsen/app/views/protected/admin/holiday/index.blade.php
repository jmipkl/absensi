@extends('protected.admin.master')

@section('title', 'Holiday Management')

@section('content')

@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Manajemen Hari Libur</small>
        </h1>
        @if (Session::has('message'))
            {{ Session::get('message') }}
        @endif
        <p class="col-md-2"><button data-toggle="modal" data-target="#newday" class="btn btn-primary btn-block" data-tooltip="tooltip" title="Tambah Hari Libur"><i class="fa fa-calendar fa-lg"></i> <i class="fa fa-plus"></i></button></p>
        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>Nama Hari Libur</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>

                                        <th>Status</th>
                                        <th width="146">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($holiday as $value)
                                    <tr>
                                        
                                        <td>{{{ $value->holiday_name }}}</td>
                                        <td>{{{ $value->startdate }}}</td>
									    <td>{{{ $value->enddate }}}</td>
										<td>{{{ $value->status == '1' ? 'BERULANG' : 'TIDAK BERULANG' }}}</td>
                                        <td>
                                            <div class="btn-group">
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$value->id}}" data-tooltip="tooltip" title="edit"><i class="fa fa-pencil-square-o"></i></button>
                                            @include('includes.holiday.edit')
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$value->id}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                            @include('includes.holiday.delete')
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach   
                                </tbody>
                            </table>
            </div>
            
    {{$holiday->links()}}
    @include('includes.holiday.create')
    </div>
</div>

@stop