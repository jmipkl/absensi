@extends('protected.admin.master')

@section('title', 'Edit Data Hari Libur')

@section('content')
	
	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Ubah Data Hari Libur</small>
        </h1>
        
 
        {{ Form::model($liburbyid, array('route' => array('admin.holiday.update', $liburbyid->id),'method' => 'PUT')) }}
             
           <div class="form-group">
                {{ Form::label('holiday_name', 'Nama Hari Libur') }}
                {{ Form::text('holiday_name', null, array('class' => 'form-control','placeholder'=>'masukkan nama hari libur')) }}
                {{ '<div>'.$errors->first('holiday_name').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('startdate', 'Tanggal Mulai') }}<br />
                {{ Form::input('date', 'startdate') }}
                {{ '<div>'.$errors->first('startdate').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('enddate', 'Tanggal Selesai') }}<br />
                {{ Form::input('date', 'enddate') }}
                {{ '<div>'.$errors->first('enddate').'</div>' }}
            </div>
                     
			<div class="form-group">
                {{ Form::label('status', 'Status') }}<br />
				{{ Form::radio('status', true) }} BERULANG <br />
				{{ Form::radio('status', false) }} TIDAK BERULANG
                {{ '<div>'.$errors->first('status').'</div>' }}
            </div>
             
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
 
    </div>
</div>

@stop