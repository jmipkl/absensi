@extends('protected.admin.master')

@section('title', 'List Users')

@section('content')
@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif
<h2>Daftar Karyawan</h2>
	<div class="col-md-2">
    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#newuser" data-tooltip="tooltip" data-placement="top" title="Tambah User"><i class="fa fa-user-plus fa-lg"></i></button>
	<br>
	</div>
	<table class="table table-bordered table-striped table-hover">
		<thead>
	        <tr>
	          <th>Akses</th>
	          <th>Email</th>
	          <th>First Name</th>
	          <th>Last Name</th>
	          <th>Action</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($users as $user)
      		<tr>
	      		<td>
	      			@if($user->inGroup($admin))
	      			<span class="label label-success" data-tooltip="tooltip" data-placement="top" title="{{$user->first_name}} is admin">{{ 'Admin' }}</span>
	      			@else
	      			<span class="label label-info" data-tooltip="tooltip" data-placement="top" title="{{$user->first_name}} is ordinary user">{{ 'User' }}</span>
	      			@endif
	      		</td>
		        <td>{{ link_to_route('admin.profiles.show', $user->email, $user->id) }}
		        </td>
		        <td>{{ $user->first_name}}</td>
		        <td>{{ $user->last_name}}</td>
		        <td>
		        	<div class="btn-group">
			        <button type="button" class="btn btn-warning" data-toggle="modal" data-tooltip="tooltip"  data-target="{{'#editmodal'}}{{$user->id}}" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></button>
					@include('includes.edit')
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$user->id}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
					@include('includes.delete')
					<a href={{ URL::to('admin/profiles/reset/'.$user->id) }}><button type="button" class="btn btn-active" data-tooltip="tooltip" data-placement="top" title="Reset Password"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></button></a>
		        	</div>
		        </td>
		     </tr>

			@endforeach
			
      	</tbody>
	</table>
	@include('includes.create')

@stop
