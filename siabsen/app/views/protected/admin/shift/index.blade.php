@extends('protected.admin.master')

@section('title', 'Employee Shift')

@section('content')

@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Jadwal Shift Pegawai</small>
        </h1>
        @if (Session::has('message'))
            {{ Session::get('message') }}
        @endif
        <p class="col-lg-2"><button data-toggle="modal" data-target="#newshift" data-tooltip="tooltip" title="Tambah Shift Baru" class="btn btn-primary btn-block">Tambah Jadwal Shift</button></p>
        @include('includes.shift.create')
        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>Nama Shift</th>
                                        <th>Hari</th>
                                        <th>Jam Masuk </th>
                                        <th>Jam Pulang </th>
                                        <th>Durasi Istirahat</th>

                                        <th width="146">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                             
                                @foreach($shift as $value)
                                
                                		<tr>                                        
                                        <td>{{{ $value->nama_shift}}}</td>
                                        <td>{{{ $value->hari }}}</td>
                                        <td>{{{ $value->jam_masuk }}}</td>
                                	    <td>{{{ $value->jam_pulang }}}</td>                                       
                                        <td>{{{ $value->durasi }}}</td>
                                        
										
                                        <td class="text-center">
                                            <div class="btn-group">
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$value->id}}" data-tooltip="tooltip" title="edit"><i class="fa fa-pencil-square-o"></i></button>
                                            @include('includes.shift.edit')
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$value->id}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                            @include('includes.shift.delete')
                                            </div>
                                        </td>
                                        
                                        
                                    </tr>
                                 
                                </tbody>
                                @endforeach
                                 
                            </table>
                             
                               
            </div>
           
 
    </div>
</div>

@stop