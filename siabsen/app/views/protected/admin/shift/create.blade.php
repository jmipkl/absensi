@extends('protected.admin.master')

@section('title', 'Tambah Shift Peagwai')

@section('content')
	    
@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Tambah Jadwal Shift Peagwai</small>
        </h1>
 
       {{ Form::open(['route' => 'admin.shift.store']) }}
             
            <div class="form-group">
                {{ Form::label('nama_shift', 'Nama Shift') }}
                {{ Form::text('nama_shift', null, array('class' => 'form-control','placeholder'=>'masukkan nama hari')) }}
                {{ '<div>'.$errors->first('nama_shift').'</div>' }}
            </div>
            
			<div class="form-group">
            	            	
                {{ Form::label('hari', 'Hari') }} :
                {{ Form::hidden('hari[]', false); }}
                {{ Form::checkbox('hari[]', 'Senin'); }} Senin
				{{ Form::checkbox('hari[]', 'Selasa'); }} Selasa
				{{ Form::checkbox('hari[]', 'Rabu'); }} Rabu
                {{ Form::checkbox('hari[]', 'Kamis'); }} Kamis
				{{ Form::checkbox('hari[]', 'Jumat'); }} Jumat
				{{ Form::checkbox('hari[]', 'Sabtu'); }} Sabtu
                
                
            </div>
            
              <div class="form-group">
                {{ Form::label('jam', 'Jam Kerja') }}
                {{ Form::select('jam', $jam, null, array('class' => 'form-control', 'placeholder' => 'Pilih Jam Kerja..')); }}
                {{ '<div>'.$errors->first('jam').'</div>' }}
            </div>
           
            {{ Form::submit('SIMPAN', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
 
    </div>
</div>
@stop