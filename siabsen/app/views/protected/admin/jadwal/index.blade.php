@extends('protected.admin.master')

@section('title', 'Employee Schedule')

@section('content')

@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Halaman
            <small>Jadwal Kerja Pegawai</small>
        </h1>
        @if (Session::has('message'))
            {{ Session::get('message') }}
        @endif
        <p class="col-lg-2"><button class="btn btn-primary btn-block" data-toggle="modal" data-target="#newjadwal" data-tooltip="tooltip" title="Tambah Jadwal User">Tambah Jadwal</button></p>
        @include('includes.jadwal.create')
        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>Nama Pegawai</th>
                                        <th>Shift</th>
                                        <th>Jam Masuk </th>
                                        <th>Jam Pulang </th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>

                                        <th width="146">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                             
                                @foreach($jadwal as $value)
                                
                                		<tr>                                        
                                        <td>{{{ $value->full_name }}}</td>
                                        <td>{{{ $value->nama_shift }}}</td>
                                        <td>{{{ $value->jam_masuk }}}</td>
                                	    <td>{{{ $value->jam_pulang }}}</td>                                       
                                        <td>{{{ $value->startdate }}}</td>
                                        <td>{{{ $value->enddate }}}</td>
                                        
										
                                        <td class="text-center">
                                            <div class="btn-group">
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{$value->id}}" data-tooltip="tooltip" title="edit"><i class="fa fa-pencil-square-o"></i></button>
                                            @include('includes.jadwal.edit')
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#delete'}}{{$value->id}}" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                            @include('includes.jadwal.delete')
                                            </div>
                                        </td>
                                        
                                        
                                    </tr>
                                 
                                </tbody>
                                @endforeach
                                 
                            </table>
                             
                               
            </div>
            
 
    </div>
</div>

@stop