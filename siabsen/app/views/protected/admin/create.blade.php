@extends('protected.admin.master')

@section('title', 'Tambah User')

@section('content')
	<h1>Form Tambah User</h1>
    
@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif
	<div class="col-md-8">
	{{ Form::open(['route' => 'admin.profiles.store']) }}

				<!-- Email field -->
							<div class="form-group">
								{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('email', $errors) }}
							</div>

							<!-- Password field -->
							<div class="form-group">
							{{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required'])}}
							{{ errors_for('password', $errors) }}
							</div>

							<!-- Password Confirmation field -->
							<div class="form-group">
							{{ Form::password('password_confirmation', ['placeholder' => 'Password Confirm', 'class' => 'form-control', 'required' => 'required'])}}

							</div>

							<!-- First name field -->
							<div class="form-group">
								{{ Form::text('first_name', null, ['placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('first_name', $errors) }}
							</div>

							<!-- Last name field -->
							<div class="form-group">
								{{ Form::text('last_name', null, ['placeholder' => 'Last Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('last_name', $errors) }}
							</div>
                            
                            <div class="form-group">
							{{ Form::label('account_type', 'Account Type:') }}
							{{ Form::select('account_type', $groups, ['class' => 'form-control']) }}
							{{ errors_for('account_type', $errors) }}
							</div>
            
            {{ Form::submit('SIMPAN', array('class' => 'btn btn-lg btn-primary btn-block')) }}
        {{ Form::close() }}
 
    </div>
@stop