<div id="{{'edit'}}{{$value->idjam}}"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<h2 class="panel-title text-center"><strong>Edit Jam Kerja</strong></h2>
    	</div>
    	<div class="modal-body">
    	    {{ Form::model($value, array('route' => array('admin.jamkerja.update', $value->idjam),'method' => 'PUT')) }}
             
         <div class="form-group">
                {{ Form::label('nama_jamkerja', 'Nama Jam Kerja') }}
                {{ Form::text('nama_jamkerja', null, array('class' => 'form-control','placeholder'=>'masukkan nama jam kerja')) }}
                {{ '<div>'.$errors->first('nama_jamkerja').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('jam_masuk', 'Jam Masuk') }} <br />
                {{ Form::input('time', 'jam_masuk') }}
                {{ '<div>'.$errors->first('jam_masuk').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('jam_pulang', 'Jam Pulang') }} <br />
                {{ Form::input('time', 'jam_pulang') }}
                {{ '<div>'.$errors->first('jam_pulang').'</div>' }}
            </div>
            
             <div class="form-group">
                {{ Form::label('start_isht', 'Mulai Istirahat') }} <br />
                {{ Form::input('time', 'start_isht') }}
                {{ '<div>'.$errors->first('start_isht').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('end_isht', 'Selesai Istirahat') }} <br />
                {{ Form::input('time', 'end_isht') }}
                {{ '<div>'.$errors->first('end_isht').'</div>' }}
            </div>
             
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
    	</div>
    </div>
</div>
</div>