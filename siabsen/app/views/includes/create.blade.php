<div id="newuser" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="newuser">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<!--<div class="panel panel-primary">
				<div class="panel-heading">-->
			<h3 class="panel-title text-center"><strong>Tambah User Baru</strong></h3>
  		</div>
  		<div class="modal-body">
		{{ Form::open(['route' => 'admin.profiles.store']) }}

				<!-- Email field -->
							<div class="form-group">
								{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required','data-tooltip'=>'tooltip','title'=>'Your Email address'])}}
								{{ errors_for('email', $errors) }}
							</div>

							<!-- Password field -->
							<div class="form-group">
							{{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required','data-tooltip'=>'tooltip','title'=>'Password'])}}
							{{ errors_for('password', $errors) }}
							</div>

							<!-- Password Confirmation field -->
							<div class="form-group">
							{{ Form::password('password_confirmation', ['placeholder' => 'Password Confirm', 'class' => 'form-control', 'required' => 'required','data-tooltip'=>'tooltip','title'=>'Password Confirmation'])}}

							</div>

							<!-- First name field -->
							<div class="form-group">
								{{ Form::text('first_name', null, ['placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required','data-tooltip'=>'tooltip','title'=>'Your Firstname'])}}
								{{ errors_for('first_name', $errors) }}
							</div>

							<!-- Last name field -->
							<div class="form-group">
								{{ Form::text('last_name', null, ['placeholder' => 'Last Name', 'class' => 'form-control', 'required' => 'required','data-tooltip'=>'tooltip','title'=>'Your Lastname'])}}
								{{ errors_for('last_name', $errors) }}
							</div>
                            
                            <div class="form-group">
							{{ Form::label('account_type', 'Account Type:') }}
							{{ Form::select('account_type', $groups, ['class' => 'form-control']) }}
							{{ errors_for('account_type', $errors) }}
							</div>
            
            {{ Form::submit('SIMPAN', array('class' => 'btn btn-primary btn-block')) }}
        {{ Form::close() }}
 			</div>
    	</div>
	</div>
</div>