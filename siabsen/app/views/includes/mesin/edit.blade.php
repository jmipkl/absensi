<div id="{{'edit'}}{{$value->id}}"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<h2 class="panel-title text-center"><strong>Edit Data Mesin</strong></h2>
    	</div>
    	<div class="modal-body">
    	    {{ Form::model($value, array('route' => array('admin.mesin.update', $value->id),'method' => 'PUT')) }}
             
            <div class="form-group">
                {{ Form::label('nama_mesin', 'Nama Mesin') }}
                {{ Form::text('nama_mesin', null, array('class' => 'form-control','placeholder'=>'masukkan nama mesin')) }}
                {{ '<div>'.$errors->first('nama_mesin').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('IP', 'Alamat IP') }}
                {{ Form::text('IP', null, array('class' => 'form-control','placeholder'=>'masukkan alamat IP')) }}
                {{ '<div>'.$errors->first('IP').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('port', 'Port') }}
                {{ Form::text('port', null, array('class' => 'form-control','placeholder'=>'masukkan port')) }}
                {{ '<div>'.$errors->first('port').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('merk', 'Jenis/Merk Mesin') }}
                {{ Form::text('merk', null, array('class' => 'form-control','placeholder'=>'masukkan jenis/merk mesin')) }}
                {{ '<div>'.$errors->first('merk').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('status', 'Status') }}<br />
				{{ Form::radio('status', true) }} AKTIF <br />
				{{ Form::radio('status', false) }} TIDAK AKTIF
                {{ '<div>'.$errors->first('status').'</div>' }}
            </div>
             
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
    	</div>
    </div>
</div>
</div>