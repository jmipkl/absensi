<div id="{{'editmodal'}}{{$user->id}}"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="{{'editmodal'}}{{$user->id}}">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<h2 class="panel-title text-center"><strong>Edit Data User</strong></h2>
    	</div>
    	<div class="modal-body">
       	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
		@endif
		{{ Form::model($user, ['method' => 'PATCH', 'route' => ['admin.profiles.update', $user->id]]) }}
		<div class="form-group">
			{{ Form::label('account_type', 'Account Type:') }}
			{{ Form::select('account_type', $groups, ['class' => 'form-control']) }}
			{{ errors_for('account_type', $errors) }}
		</div>
		<div class="form-group">
			{{ Form::label('email', 'Email:') }}
			{{ Form::email('email', null, ['class' => 'form-control']) }}
			{{ errors_for('email', $errors) }}
		</div>
		<div class="form-group">
			{{ Form::label('first_name', 'First Name:') }}
			{{ Form::text('first_name', null, ['class' => 'form-control']) }}
			{{ errors_for('first_name', $errors) }}
		</div>

		<!-- last_name Field -->
		<div class="form-group">
			{{ Form::label('last_name', 'Last Name:') }}
			{{ Form::text('last_name', null, ['class' => 'form-control']) }}
			{{ errors_for('last_name', $errors) }}
		</div>

		<!-- Password field -->
		<div class="form-group">
			{{ Form::label('password', 'Password:') }}
			{{ Form::password('password', ['class' => 'form-control']) }}
			<p class="help-block">Leave password blank to NOT edit the password.</p>
			{{ errors_for('password', $errors) }}
		</div>

		<!-- Password Confirmation field -->
		<div class="form-group">
			{{ Form::label('password_confirmation', 'Repeat Password:') }}
			{{ Form::password('password_confirmation', ['class' => 'form-control'] )}}
		</div>
		<div class="form-group">
			{{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
		</div>
		{{ Form::close() }}
    	</div>
    </div>
  </div>
</div>