<div id="{{'delete'}}{{$value->id}}"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-body">
            <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
             <a href="{{ URL::to('admin/holiday/destroy/'.$value->id) }}" class="btn btn-danger danger">Delete</a>
        </div>
    </div>
  </div>
</div>