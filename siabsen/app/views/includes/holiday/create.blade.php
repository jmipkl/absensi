<div id="newday"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <h3 class="panel-title text-center"><strong>Tambah Hari Libur</strong></h3>
        </div>
        <div class="modal-body">
        {{ Form::open(['route' => 'admin.holiday.store']) }}
             
            <div class="form-group">
                {{ Form::label('holiday_name', 'Nama Hari Libur') }}
                {{ Form::text('holiday_name', null, array('class' => 'form-control','placeholder'=>'masukkan nama hari libur')) }}
                {{ '<div>'.$errors->first('holiday_name').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('startdate', 'Tanggal Mulai') }}<br />
                {{ Form::input('date', 'startdate') }}
                {{ '<div>'.$errors->first('startdate').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('enddate', 'Tanggal Selesai') }}<br />
                {{ Form::input('date', 'enddate') }}
                {{ '<div>'.$errors->first('enddate').'</div>' }}
            </div>
                     
            <div class="form-group">
                {{ Form::label('status', 'Status') }}<br />
                {{ Form::radio('status', true) }} BERULANG <br />
                {{ Form::radio('status', false) }} TIDAK BERULANG
                {{ '<div>'.$errors->first('status').'</div>' }}
            </div>
             
           
            {{ Form::submit('SIMPAN', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}        
        </div>
    </div>
  </div>
</div>