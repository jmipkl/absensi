<div id="{{'edit'}}{{$value->id}}"class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<h2 class="panel-title text-center"><strong>Edit Jam Kerja</strong></h2>
    	</div>
    	<div class="modal-body">
    	    {{ Form::model($value, array('route' => array('admin.shift.update', $value->id),'method' => 'PUT')) }}
             
           <div class="form-group">
                {{ Form::label('nama_shift', 'Nama Shift') }}
                {{ Form::text('nama_shift', null, array('class' => 'form-control','placeholder'=>'masukkan nama hari')) }}
                {{ '<div>'.$errors->first('nama_shift').'</div>' }}
            </div>
            
            <div class="form-group">
                 {{ Form::label('hari', 'Hari') }} :
                {{ Form::checkbox('hari[]', 'Senin'); }} Senin
                {{ Form::checkbox('hari[]', 'Selasa'); }} Selasa
                {{ Form::checkbox('hari[]', 'Rabu'); }} Rabu
                {{ Form::checkbox('hari[]', 'Kamis'); }} Kamis
                {{ Form::checkbox('hari[]', 'Jumat'); }} Jumat
                {{ Form::checkbox('hari[]', 'Sabtu'); }} Sabtu   
            </div>
             
              <div class="form-group">
                {{ Form::label('jam', 'Jam Kerja') }}
                {{ Form::select('jam', $jam, null, array('class' => 'form-control', 'placeholder' => 'Pilih Jam Kerja..')); }}
                {{ '<div>'.$errors->first('jam').'</div>' }}
            </div>
           
            {{ Form::submit('UPDATE', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
    	</div>
    </div>
</div>
</div>