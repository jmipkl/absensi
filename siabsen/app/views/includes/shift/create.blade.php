<div id="newshift" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="newuser">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header modal-primary">
    		<!--<div class="panel panel-primary">
				<div class="panel-heading">-->
			<h3 class="panel-title text-center"><strong>Tambah Shift</strong></h3>
  		</div>
  		<div class="modal-body">
  			      {{ Form::open(['route' => 'admin.shift.store']) }}
             
            <div class="form-group">
                {{ Form::label('nama_shift', 'Nama Shift') }}
                {{ Form::text('nama_shift', null, array('class' => 'form-control','placeholder'=>'masukkan nama hari')) }}
                {{ '<div>'.$errors->first('nama_shift').'</div>' }}
            </div>
            
             <div class="form-group">
                            
                {{ Form::label('hari', 'Hari') }} :
                {{ Form::hidden('hari[]', false); }}
                {{ Form::checkbox('hari[]', 'Senin'); }} Senin
                {{ Form::checkbox('hari[]', 'Selasa'); }} Selasa
                {{ Form::checkbox('hari[]', 'Rabu'); }} Rabu
                {{ Form::checkbox('hari[]', 'Kamis'); }} Kamis
                {{ Form::checkbox('hari[]', 'Jumat'); }} Jumat
                {{ Form::checkbox('hari[]', 'Sabtu'); }} Sabtu
                
                
            </div>
            
              <div class="form-group">
                {{ Form::label('jam', 'Jam Kerja') }}
                {{ Form::select('jam', $jam, null, array('class' => 'form-control', 'placeholder' => 'Pilih Jam Kerja..')); }}
                {{ '<div>'.$errors->first('jam').'</div>' }}
            </div>
           
                {{ Form::submit('SIMPAN', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
        
  		</div>

  	</div>
  </div>
</div>