<div id="newjadwal" class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="newuser">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <!--<div class="panel panel-primary">
                <div class="panel-heading">-->
            <h3 class="panel-title text-center"><strong>Tambah Shift</strong></h3>
        </div>
        <div class="modal-body">
            {{ Form::open(['route' => 'admin.jadwal.store']) }}
             
            <div class="form-group">
                {{ Form::label('idpeg', 'Nama Pegawai') }}
                {{ Form::select('idpeg', $idpeg, null, array('class' => 'form-control', 'placeholder' => 'Pilih Nama Pegawai..')); }}
                {{ '<div>'.$errors->first('idpeg').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('idshift', 'Shift') }}
                {{ Form::select('idshift', $idshift, null, array('class' => 'form-control', 'placeholder' => 'Pilih Jenis Shift..')); }}
                {{ '<div>'.$errors->first('idshift').'</div>' }}
            </div>
            
            <div class="form-group">
                {{ Form::label('startdate', 'Tanggal Mulai') }}<br />
                {{ Form::input('date', 'startdate') }}
                {{ '<div>'.$errors->first('startdate').'</div>' }}
            </div>
            
			<div class="form-group">
                {{ Form::label('enddate', 'Tanggal Selesai') }}<br />
                {{ Form::input('date', 'enddate') }}
                {{ '<div>'.$errors->first('enddate').'</div>' }}
            </div>
           
            {{ Form::submit('SIMPAN', array('class' => 'btn btn-lg btn-primary btn-block')) }}
 
        {{ Form::close() }}
        </div>

    </div>
  </div>
</div>