@extends('master')

@section('title', 'Login')

@section('content')


	<div class="container">
	    <div class="row">
			<div class="col-md-4 col-md-offset-4">
	    		<div class="panel panel-primary">
				  	<div class="panel-heading">
				    	<h3 class="panel-title ">Login</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['route' => 'sessions.store']) }}
	                    <fieldset>

	                    	@if (Session::has('flash_message'))
								<p style="paddsing:5px" class="bg-success text-success">{{ Session::get('flash_message') }}</p>
							@endif

							@if (Session::has('error_message'))
								<p style="padding:5px" class="bg-danger text-danger">{{ Session::get('error_message') }}</p>
							@endif

				    	  	<!-- Email field -->
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon info"><i class="fa fa-at"></i></div>
								{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('email', $errors) }}
								</div>
							</div>

				    		<!-- Password field -->
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-addon info"><i class="fa fa-lock"></i></div>
								{{ Form::password('password', ['placeholder' => 'Password','class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('password', $errors) }}
								</div>
							</div>

							<!-- Remember me field -->
				    		<div class="form-group">
								<div class="checkbox">
									{{ Form::label('remember',' ')}}
									{{ Form::checkbox('remember', 'remember') }} Remember Me?
								</div>
				    	    </div>

				    		<!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Login', ['class' => 'btn btn btn-lg btn-primary btn-block']) }}
							</div>
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
				<div style="text-align:center">
					<p><a href="/forgot_password">Forgot Password?</a></p>

					<p><strong>Standard User:</strong> user@user.com<br>
					<strong>Standard User Password:</strong> sentryuser</p>

					<p><strong>Admin User:</strong> admin@admin.com<br>
					<strong>Admin Password:</strong> sentryadmin</p>
				</div>


			</div>
		</div>
	</div>

@stop