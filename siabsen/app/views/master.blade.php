<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<title>@yield('title') - Sistem Informasi Absensi</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Stylesheets -->
	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::style('assets/font-awesome-4.3.0/css/font-awesome.min.css') }}

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--Js config-->
	{{ HTML::script('assets/js/jquery-1.11.0.js') }}
	{{ HTML::script('assets/js/bootstrap.min.js') }}
	<script type="text/javascript">
    $(function () {$('[data-tooltip="tooltip"]').tooltip()});
    $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()});
  	</script>


</head>
<body>

	<header>

		<nav class="navbar navbar-inverse" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <a class="navbar-brand" href="/">Sistem Informasi Absensi</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="{{ set_active('/') }}"><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
		        <li class="{{ set_active('about') }}"><a href="/about"><span class="fa fa-at fa-lg" aria-hidden="true"></span> About</a></li>
		        <li class="{{ set_active('contact') }}"><a href="/contact"><i class="fa fa-phone-square fa-lg"></i> Contact</a></li>
		        <li class="{{ set_active('userProtected') }}"><a href="/userProtected">Registered Users Only</a></li>
		      </ul>

		      <ul class="nav navbar-nav navbar-right">
		      	@if (!Sentry::check())
					<li class="{{ set_active('register') }}"><a href="/register"><i class="fa fa-user-plus"></i> Register</a></li>
					<li class="{{ set_active('login') }}"><a href="/login"><i class="fa fa-sign-in fa-lg"></i> Login</a></li>
				@else
					<li class="{{ set_active('profiles') }}"><a href="/profiles/{{Sentry::getUser()->id}}"><i class="fa fa-user"></i> My Profile</a></li>
					<li><a href="/logout">Logout</a></li>
				@endif
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>




	</header>

	<div class="container">
		@yield('content')
	</div>
</body>
</html>