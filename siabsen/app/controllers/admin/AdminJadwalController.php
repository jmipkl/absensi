<?php


class AdminJadwalController extends \BaseController {


public function index()
{
   	$jadwal = DB::select
	('SELECT jadwal.id, shift.nama_shift, jam_masuk, jam_pulang, 						
	jadwal.startdate, jadwal.enddate, CONCAT(users.first_name," ", users.last_name) full_name 
	FROM jamkerja 
	INNER JOIN shift on jamkerja.idjam=shift.idjam
	INNER JOIN jadwal on shift.id=jadwal.idshift
	INNER JOIN users on jadwal.idpeg=users.id');
	$jadwal = 
		[
		    'jadwal' => $jadwal
	    ];		
	$idpeg = DB::table('users')->lists( 'first_name', 'id');
    $idpeg =
    	[
        'idpeg' => $idpeg
    	];
	$idshift = DB::table('shift')->lists( 'nama_shift', 'id');
    $idshift =
    	[
        'idshift' => $idshift
    	];
    return View::make('protected.admin.jadwal.index', $jadwal, $idpeg)->with('idshift',$idshift);
}

public function create()
{
	$idpeg = DB::table('users')->lists( 'first_name', 'id');
    $idpeg =
    	[
        'idpeg' => $idpeg
    	];
	$idshift = DB::table('shift')->lists( 'nama_shift', 'id');
    $idshift =
    	[
        'idshift' => $idshift
    	];	
    return View::make('protected.admin.jadwal.create',$idpeg, $idshift);
}
 
public function store()
{
     
  $rules = array(
		 'idpeg' => 'required',
		 'idshift' => 'required',
		 
	

	);

	
		$validator = Validator::make(Input::all(), $rules);
				

		if ($validator->fails()) {	
			return Redirect::to('admin/jadwal/create')->withErrors($validator)->withInput();
		} else {
		DB::table('jadwal')->insert(
    		array(
					'idpeg' => Input::get('idpeg'),
	    			'idshift' =>  Input::get('idshift'),      
					'startdate' => Input::get('startdate'),
					'enddate' => Input::get('enddate'),
					
    			)
			);
		
    	
        return Redirect::to('admin/jadwal')->withFlashMessage('Data Jadwal Kerja Berhasil Ditambahkan!');
		}
		
	}
	
	public function edit($id)
	{
		$jadwalbyid = DB::table('jadwal')->where('id',$id)->first();
		$jadwalbyid = 
		[
		    'jadwalbyid' => $jadwalbyid,

	    ];
		
		$idshift = DB::table('shift')->lists( 'nama_shift', 'id');
   		 $idshift =
    	[
        'idshift' => $idshift
    	];	
		
        return View::make('protected.admin.jadwal.edit', $jadwalbyid,$idshift);
	}


	public function update($id)
	{
		$rules = array(
		
		 'idshift' => 'required',
		 'startdate' => 'required',
 		 'enddate' => 'required',
		 
	
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return Redirect::to('admin/jadwal/edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('jadwal')
            ->where('id', $id)
            ->update(array(
            		
	    			'idshift' =>  Input::get('idshift'),      
					'startdate' => Input::get('startdate'),
					'enddate' => Input::get('enddate'),
            	));
	    return Redirect::to('admin/jadwal')->withFlashMessage('Data Jadwal Berhasil Diubah!');

		}
	}

	public function destroy($id)
	{
		DB::table('jadwal')->where('id', '=', $id)->delete();
		 return Redirect::to('admin/jadwal')->withFlashMessage('Data Jadwal Berhasil Dihapus!');

	}

	
}