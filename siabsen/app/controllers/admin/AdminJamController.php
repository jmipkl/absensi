<?php



class AdminJamController extends \BaseController {

public function index()
{
   	$jamkerja = DB::table('jamkerja')->paginate(5);
	$jamkerja = 
		[
		    'jamkerja' => $jamkerja
	    ];
    
    return View::make('protected.admin.jamkerja.addjam', $jamkerja);
}

public function create()
{
    return View::make('protected.admin.jamkerja.createjam');
}
 
public function store()
{
     
  $rules = array(
		 'nama_jamkerja' => 'required',
		'jam_masuk' => 'required',
		'jam_pulang' => 'required',
		'start_isht' => 'required',
		'end_isht' => 'required',

	);


		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			return Redirect::to('admin/jamkerja/create')->withErrors($validator)->withInput();
		} else {
		DB::table('jamkerja')->insert(
    		array(
	    			'nama_jamkerja' => Input::get('nama_jamkerja'),
	            	'jam_masuk' => Input::get('jam_masuk'),
	            	'jam_pulang' => Input::get('jam_pulang'),
					'start_isht' => Input::get('start_isht'),
					'end_isht' => Input::get('end_isht'),

    			)
			);
		
    	
        return Redirect::to('admin/jamkerja')->withFlashMessage('Data Jam Kerja Berhasil Ditambahkan!');
		}
		
	}
	
	public function edit($id)
	{
		$jambyid = DB::table('jamkerja')->where('idjam',$id)->first();
		$jambyid = 
		[
		    'jambyid' => $jambyid
	    ];
        return View::make('protected.admin.jamkerja.editjam', $jambyid);
	}


	public function update($id)
	{
		$rules = array(
		
		 'nama_jamkerja' => 'required',
		'jam_masuk' => 'required',
		'jam_pulang' => 'required',
		'start_isht' => 'required',
		'end_isht' => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return Redirect::to('admin/jamkerja/edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('jamkerja')
            ->where('idjam', $id)
            ->update(array(
            		'nama_jamkerja' => Input::get('nama_jamkerja'),
	            	'jam_masuk' => Input::get('jam_masuk'),
	            	'jam_pulang' => Input::get('jam_pulang'),
					'start_isht' => Input::get('start_isht'),
					'end_isht' => Input::get('end_isht'),
            	));
	    return Redirect::to('admin/jamkerja')->withFlashMessage('Data Jam Kerja Berhasil Diubah!');

		}
	}

	public function destroy($id)
	{
		DB::table('jamkerja')->where('idjam', '=', $id)->delete();
		 return Redirect::to('admin/jamkerja')->withFlashMessage('Data Jam Kerja Berhasil Dihapus!');

	}

	
}