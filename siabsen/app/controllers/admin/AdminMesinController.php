<?php



class AdminMesinController extends \BaseController {

public function index()
{
   	$mesin = DB::table('mesin')->paginate(5);
	$mesin = 
		[
		    'mesin' => $mesin
	    ];
    
    return View::make('protected.admin.mesin.index', $mesin);
}

public function create()
{
    return View::make('protected.admin.mesin.create');
}
 
public function store()
{
     
  $rules = array(
		 'status' => 'integer',
		'nama_mesin' => 'required',
		'IP' => 'required',
		'port' => 'required|min:2',
		'merk' => 'required',

	);


		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			return Redirect::to('admin/mesin/create')->withErrors($validator)->withInput();
		} else {
		DB::table('mesin')->insert(
    		array(
	    			'nama_mesin' => Input::get('nama_mesin'),
	            	'IP' => Input::get('IP'),
	            	'port' => Input::get('port'),
					'merk' => Input::get('merk'),
	            	'status' => Input::get('status')
    			)
			);
		
    	
        return Redirect::to('admin/mesin')->withFlashMessage('Data Mesin Berhasil Ditambahkan!');
		}
		
	}
	
	public function edit($id)
	{
		$mesinbyid = DB::table('mesin')->where('id',$id)->first();
		$mesinbyid = 
		[
		    'mesinbyid' => $mesinbyid
	    ];
        return View::make('protected.admin.mesin.edit', $mesinbyid);
	}


	public function update($id)
	{
		$rules = array(
		
		'nama_mesin' => 'required',
		'IP' => 'required',
		'port' => 'required|min:2',
		'merk' => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return Redirect::to('admin/mesin/edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('mesin')
            ->where('id', $id)
            ->update(array(
            		'nama_mesin' => Input::get('nama_mesin'),
	            	'IP' => Input::get('IP'),
	            	'port' => Input::get('port'),
					'merk' => Input::get('merk'),
	            	'status' => Input::get('status')
            	));
	    return Redirect::to('admin/mesin')->withFlashMessage('Data Mesin Berhasil Diubah!');

		}
	}

	public function destroy($id)
	{
		DB::table('mesin')->where('id', '=', $id)->delete();
		 return Redirect::to('admin/mesin')->withFlashMessage('Data Mesin Berhasil Dihapus!');

	}

	
}