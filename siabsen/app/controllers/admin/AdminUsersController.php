<?php

use basicAuth\Repo\UserRepositoryInterface;
use basicAuth\formValidation\AdminUsersEditForm;
use basicAuth\formValidation\AdminUsersCreateForm;


class AdminUsersController extends \BaseController {

	/**
	 * @var $user
	 */
	protected $user;
	
	private $AdminUsersCreateForm;
	protected $adminUsersEditForm;

	function __construct(UserRepositoryInterface $user, AdminUsersCreateForm $AdminUsersCreateForm, AdminUsersEditForm $adminUsersEditForm)
	{
		$this->user = $user;
		$this->AdminUsersCreateForm = $AdminUsersCreateForm;
		$this->adminUsersEditForm = $adminUsersEditForm;
	}


	/**
	* @var adminUsersEditForm
	*/
	


	/**
	* @param AdminUsersEditForm $AdminUsersEditForm
	*/



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->user->getAll();
		$admin = Sentry::findGroupByName('Admins');
		//$user_group = $user->getGroups()->first()->id;
		$groups = Sentry::findAllGroups();
		$array_groups = [];

		foreach ($groups as $group) {
			$array_groups = array_add($array_groups, $group->id, $group->name);
		}
		return View::make('protected.admin.list_users',['groups' => $array_groups])->withUsers($users)->withAdmin($admin);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->user->find($id);

		$user_group = $user->getGroups()->first()->name;

		$groups = Sentry::findAllGroups();


		return View::make('protected.admin.show_user')->withUser($user)->withUserGroup($user_group);

	}

	public function create()
	{

		$groups = Sentry::findAllGroups();

		$array_groups = [];

		foreach ($groups as $group) {
			$array_groups = array_add($array_groups, $group->id, $group->name);
		}

		return View::make('protected.admin.create', ['groups' => $array_groups]);
	}
	
 
	public function store()
	{
    	$input = Input::only('email', 'password', 'password_confirmation', 'first_name', 'last_name','account_type');

		$this->AdminUsersCreateForm->validate($input);

		$input = Input::only('email', 'password', 'first_name', 'last_name');
		$input = array_add($input, 'activated', true);

		$user = $this->user->create($input);
		$akun=Input::get('account_type');
		if($akun==2){		
		// Find the group using the group name
    	$usersGroup = Sentry::findGroupByName('Admins');

    	// Assign the group to the user
    	$user->addGroup($usersGroup);
		
		}else if($akun==1){		
		// Find the group using the group name
    	$usersGroup = Sentry::findGroupByName('Users');

    	// Assign the group to the user
    	$user->addGroup($usersGroup);
		}

		return Redirect::to('admin/profiles')->withFlashMessage('User Successfully Created!');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);

		$groups = Sentry::findAllGroups();

		$user_group = $user->getGroups()->first()->id;

		$array_groups = [];

		foreach ($groups as $group) {
			$array_groups = array_add($array_groups, $group->id, $group->name);
		}

		return View::make('protected.admin.edit_user', ['user' => $user, 'groups' => $array_groups, 'user_group' =>$user_group]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$user = $this->user->find($id);


		if (! Input::has("password"))
		{
			$input = Input::only('account_type' , 'email', 'first_name', 'last_name');

			$this->adminUsersEditForm->excludeUserId($user->id)->validate($input);

			$input = array_except($input, ['account_type']);

			$user->fill($input)->save();

			$this->user->updateGroup($id, Input::get('account_type'));


			return Redirect::to('admin/profiles')->withFlashMessage('User has been updated successfully!');
		}

		else
		{
			$input = Input::only('account_type', 'email', 'first_name', 'last_name', 'password', 'password_confirmation');

			$this->adminUsersEditForm->excludeUserId($user->id)->validate($input);

			$input = array_except($input, ['account_type', 'password_confirmation']);

			$user->fill($input)->save();

			$user->save();

			$this->user->updateGroup($id, Input::get('account_type'));

			return Redirect::to('admin/profiles')->withFlashMessage('User (and password) has been updated successfully!');

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	
    try
    {
        $user = Sentry::findUserById($id);
        $user->delete();
     }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
    {
        Session::flash('message', 'User tidak ditemukan.');
        return Redirect::to('admin/profiles');
    }
    	return Redirect::to('admin/profiles')->withFlashMessage('User Berhasil Dihapus');
	
	
	
  /* DB::table('users')->where('id', '=', $id)->delete();
		Session::flash('message', 'Data Berhasil Dihapus');
		return Redirect::to('admin/profiles');*/
	}

	public function resetPass($id){
	try
	{
    	// Find the user using the user id
    $user = Sentry::findUserById($id);

    // Update the user details
    $user->password = Securepass::generateHuman();
    
    // Update the user
    	if ($user->save())
    	{
        	return Redirect::to('admin/profiles')->withFlashMessage('Password untuk user '.$user->first_name.' berhasil direset!');
    	}
    	else
    	{
        	return Redirect::to('admin/profiles')->withFlashMessage('Terjadi kesalahan pada reset password');
    	}
	}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
	{
    	echo 'User with this login already exists.';
	}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
    	echo 'User was not found.';
	}
				
	}
}