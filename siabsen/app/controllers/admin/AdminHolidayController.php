<?php



class AdminHolidayController extends \BaseController {

public function index()
{
   	$holiday = DB::table('holiday')->paginate(5);
	$holiday = 
		[
		    'holiday' => $holiday
	    ];
    
    return View::make('protected.admin.holiday.index', $holiday);
}

public function create()
{
    return View::make('protected.admin.holiday.create');
}
 
public function store()
{
     
  $rules = array(
		 'status' => 'integer',
		'holiday_name' => 'required',
		

	);


		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			return Redirect::to('admin/holiday/create')->withErrors($validator)->withInput();
		} else {
		DB::table('holiday')->insert(
    		array(
	    			'holiday_name' => Input::get('holiday_name'),
	            	'startdate' => Input::get('startdate'),
					'enddate' => Input::get('enddate'),
	            	'status' => Input::get('status')
    			)
			);
		
    	
        return Redirect::to('admin/holiday')->withFlashMessage('Data Hari Libur Berhasil Ditambahkan!');
		}
		
	}
	
	public function edit($id)
	{
		$liburbyid = DB::table('holiday')->where('id',$id)->first();
		$liburbyid = 
		[
		    'liburbyid' => $liburbyid
	    ];
        return View::make('protected.admin.holiday.edit', $liburbyid);
	}


	public function update($id)
	{
		$rules = array(
		
		'status' => 'integer',
		'holiday_name' => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return Redirect::to('admin/holiday/edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('holiday')
            ->where('id', $id)
            ->update(array(
            		'holiday_name' => Input::get('holiday_name'),
	            	'startdate' => Input::get('startdate'),
					'enddate' => Input::get('enddate'),
	            	'status' => Input::get('status')
            	));
	    return Redirect::to('admin/holiday')->withFlashMessage('Data Hari Libur Berhasil Diubah!');

		}
	}

	public function destroy($id)
	{
		DB::table('holiday')->where('id', '=', $id)->delete();
		 return Redirect::to('admin/holiday')->withFlashMessage('Data Hari Libur Berhasil Dihapus!');

	}

	
}