<?php


class AdminShiftController extends \BaseController {


public function index()
{
   	$shift = DB::select('SELECT id,nama_shift, hari, jamkerja.jam_masuk, jamkerja.jam_pulang, TIME(jamkerja.end_isht-jamkerja.start_isht) as durasi FROM shift INNER JOIN jamkerja on shift.idjam=jamkerja.idjam');
	$shift = 
		[
		    'shift' => $shift
	    ];
	    		$jam = DB::table('jamkerja')->lists( 'nama_jamkerja', 'idjam');
    $jam =
    	[
        'jam' => $jam
    	];		
	
    return View::make('protected.admin.shift.index', $shift,$jam);
}

public function create()
{
	$jam = DB::table('jamkerja')->lists( 'nama_jamkerja', 'idjam');
    $jam =
    	[
        'jam' => $jam
    	];
    return View::make('protected.admin.shift.create',$jam);
}
 
public function store()
{
     
  $rules = array(
		 'nama_shift' => 'required',
		 'hari' => 'required',
	

	);

	
		$validator = Validator::make(Input::all(), $rules);
				

		if ($validator->fails()) {	
			return Redirect::to('admin/shift/create')->withErrors($validator)->withInput();
		} else {
		DB::table('shift')->insert(
    		array(
					'nama_shift' => Input::get('nama_shift'),
	    			'hari' =>  implode(',', Input::get('hari')),      
					'idjam' => Input::get('jam')
					
    			)
			);
		
    	
        return Redirect::to('admin/shift')->withFlashMessage('Data Jadwal Shift Berhasil Ditambahkan!');
		}
		
	}
	
	public function edit($id)
	{
		$shiftbyid = DB::table('shift')->where('id',$id)->first();
		$shiftbyid = 
		[
		    'shiftbyid' => $shiftbyid,

	    ];
		$jam = DB::table('jamkerja')->lists( 'nama_jamkerja', 'idjam');
    $jam =
    	[
        'jam' => $jam
    	];
        return View::make('protected.admin.shift.edit', $shiftbyid,$jam);
	}


	public function update($id)
	{
		$rules = array(
		
		 'nama_shift' => 'required',
		 'hari' => 'required',

		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {	
			echo "string";
			return Redirect::to('admin/shift/edit/'.$id)->withErrors($validator)->withInput();
		} else {
		DB::table('shift')
            ->where('id', $id)
            ->update(array(
            		'nama_shift' => Input::get('nama_shift'),
	    			'hari' => implode(', ',Input::get('hari')),
					'idjam' => Input::get('jam')
            	));
	    return Redirect::to('admin/shift')->withFlashMessage('Data shift Berhasil Diubah!');

		}
	}

	public function destroy($id)
	{
		DB::table('shift')->where('id', '=', $id)->delete();
		 return Redirect::to('admin/shift')->withFlashMessage('Data shift Berhasil Dihapus!');

	}

	
}