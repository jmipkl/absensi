<?php namespace basicAuth\formValidation;

use Laracasts\Validation\FormValidator;

class AdminUsersCreateForm extends FormValidator {

 protected $rules = [
		 'account_type' => 'integer|between:1,2',
   		 'email' => 'required|email|unique:users',
		'first_name' => 'required',
		'last_name' => 'required',
		'password' => 'confirmed|min:6',
	];

}


