<?php include("header.php");?>
<body>
		<nav class="navbar navbar-inverse navbar-static-top">
		<p class="navbar-text">Signed in as <a href="#" class="navbar-link"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Admin</a></p>
		<p class="navbar-text pull-right"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Logout</p>
		</nav>
<div class="col-md-2">
	<div class="panel panel-primary">
		<div class="panel-heading">
    		<h3 class="panel-title">Admin Dashboard</h3>
  		</div>
  		<div class="panel-body">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation"><a href="#">Karyawan</a></li>
  				<li role="presentation"><a href="#">Profile</a></li>
  				<li role="presentation"><a href="#">Messages</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="col-md-8">
	<table class="table table-striped table-hover">
		<tr class="success">
			<td>#</td>
			<td>Nama</td>
			<td>Alamat</td>
			<td>Email</td>
			<td>Handphone</td>
			<td>Action</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Aditya Marselvada</td>
			<td>Malang</td>
			<td>adit@yahoo.com</td>
			<td>0823123120</td>
			<td>
				<button type="button" class="btn btn-warning" data-toggle="modal" data-tooltip="tooltip"  data-target=".bs-example-modal-sm" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span></button>
				<button type="button" class="btn btn-danger" data-tooltip="tooltip" data-placement="top" title="Hapus"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<button type="button" class="btn btn-active" data-tooltip="tooltip" data-placement="top" title="Reset Password"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></button>
			</td>
		</tr>
	</table>
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="panel panel-primary">
      	<div class="panel-heading">EDIT</div>
      	<div class="panel-body">
    		<table class="table table-hover">
				<tr>
					<td>Nama</td><td><input type="text" class="form-control" id="nama" value="Aditya Marselvada"></td>
				</tr>
				<tr>
					<td>Alamat</td><td><input type="text" class="form-control" id="alamat" value="Malang"></td>
				</tr>
				<tr>
					<td>Email</td><td><input type="text" class="form-control" id="email" value="Adit@yahoo.com"></td>
				</tr>
				<tr>
					<td>Handphone</td><td><input type="text" class="form-control" id="hp" value="081229412"></td>
				</tr>
				<tr>
					<td></td><td><input type="submit" class="btn btn-primary" id="submit" value="Simpan"></td>
				</tr>
			</table>
  		</div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-2"></div>
</body>